blynk-mobile-development-for-arduino

https://gitlab.com/bonsi/blynk-mobile-development-for-arduino

https://www.udemy.com/course/blynk-iot/

---

# Section 1: Introduction

https://github.com/futureshocked/Arduino-mobile-development-with-Blynk


# Section 2: Getting started with Blynk

## 7. 02.20 The Arduino Nano 33 IoT

- Install Arduino Nano 33 IOT board support under the board manager (search for "nano 33 iot")
- Install library for Blynk under Sketch, Include library, Manage libraries

## 9. 02.40 Create a new Blynk app

- https://examples.blynk.cc
- File -> Examples -> Blynk

## 10. 02.50 How to use a virtual pin

https://docs.blynk.cc/#blynk-main-operations-virtual-pins

https://www.arduino.cc/en/Guide/NANO33IoT

## 11. 02.60 Replace the Arduino Nano 33 IoT with an ESP32

# Section 3: Blynk with two or more devices

## 12. 03.10 Dual-device example with Arduino Nano 33 IoT and ESP32


---
temporary skipping to the section where we setup a private blynk server
---

# Section 5: Setup a private Blynk server on a Raspberry Pi Zero W

## 38. 05.10 Prepare the Raspberry Pi Zero W with the OS

- https://www.raspberrypi.org/downloads/raspberry-pi-os/
  - Download the Raspberry Pi OS (32-bit) Lite (without X and such)
- https://www.raspberrypi.org/documentation/remote-access/ssh/
  - Save an empty file named `ssh` in `/boot`
- https://www.raspberrypi.org/documentation/configuration/wireless/headless.md
  - Save a valid file named `wpa_supplicant.conf` in `/boot`


```conf
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=<Insert 2 letter ISO 3166-1 country code here>

network={
 ssid="<Name of your wireless LAN>"
 psk="<Password for your wireless LAN>"
}
```

## 39. 05.12 Boot the Raspberry Pi for the first time and basic configuration

- `sudo raspi-config`
- Set hostname
- Expand filesystem
- Reboot

## 40. 05.20 Optimise OS for RPi Zero

- Memory split
  - `sudo raspi-config` -> Performance Options -> GPU Memory -> 16 MB
- Install ZRAM: Automatically detects the number of CPU cores to allocate to ZRAM computation, disables existing swap and enables ZRAM swap.
  - https://github.com/novaspirit/rpi_zram
- Disable HDMI
  - `/usr/bin/tvservice -o`
  - This will have to be run every boot:
    - `sudo nano /etc/rc.local`
    - add `/usr/bin/tvservice -o` before `exit 0`

## 41. 05.30 Install the Blynk server

```bash
sudo apt update
sudo apt install openjdk-8-jdk openjdk-8-jre
```



```bash
# debian has switched to nftables by default, so we're switching back to the old iptables framework
sudo update-alternatives --set iptables /usr/sbin/iptables-legacy
sudo update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy
sudo update-alternatives --set arptables /usr/sbin/arptables-legacy
sudo update-alternatives --set ebtables /usr/sbin/ebtables-legacy
# and now these rules will work
sudo iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 8080
sudo iptables -t nat -A PREROUTING -p tcp --dport 443 -j REDIRECT --to-port 9443

cd
mkdir -p blynk-server/data
cd blynk-server
wget https://github.com/blynkkk/blynk-server/releases/download/v0.41.14/server-0.41.14-java8.jar

java -jar server-0.41.14-java8.jar -dataFolder data/
```

https://pizerow.home.localdomain:9443/admin

# Section 6: Private Blynk server configuration

## 43. 06.10 Introduction to Blynk server configuration

- copy the contents of `server.properties` from the webIface
- create a new file `server.properties` in the root blynk dir (`/home/pi/blynk-server/`)
- customize
- `java -jar server-0.41.14-java8.jar -dataFolder /home/pi/blynk-server/data/ -serverConfig /home/pi/blynk-server/server.properties`

## 44. 06.20 Setup autostart

To enable server auto restart find /etc/rc.local file and add:

`java -jar /home/pi/server-0.41.13-java8.jar -dataFolder /home/pi/Blynk &`

Or if the approach above doesn't work, execute
`crontab -e`

add the following line

`@reboot java -jar /home/pi/server-0.41.13-java8.jar -dataFolder /home/pi/Blynk &`

save and exit.

## 45. 06.30 Enable email notifications

- copy the contents of `mail.properties` from the webIface
- create a new file `mail.properties` in the root blynk dir (`/home/pi/blynk-server/`)
- customize
- `java -jar server-0.41.14-java8.jar -dataFolder /home/pi/blynk-server/data/ -serverConfig /home/pi/blynk-server/server.properties` (no need to explicitely specify the `mail.properties` config)

## 46. 06.50 Security with self-signed certificates

```bash
cd 
mkdir certs
openssl -req -x509 -nodes -days 1825 -newkey rsa:2048 -keyout server.key -out server.crt
openssl pkcs8 -topk8 -inform PEM -outform PEM -in server.key -out server.pem

cd
cd blynk-server
vi server.properties
# set the following params:
server.ssl.key=/home/pi/blynk-server/certs/server.key
server.ssl.key.pass=
server.ssl.cert=/home/pi/blynk-server/certs/server.crt

```

## 47. 06.60 Access your Blynk server from the Internet with port forwarding

## 48. 06.70 The properties file

More options:
https://github.com/blynkkk/blynk-server#advanced-local-server-setup


---
continuing where I left off in section 3
---

# Section 3: Blynk with two or more devices

## 14. 03.30 Sketch for the Arduino and the ESP32




