#define BLYNK_PRINT Serial

//#include <SPI.h>/
//#include <WiFiNINA.h>
//#include <BlynkSimpleWiFiNINA.h>

#include <WiFi.h>
//#include <WifiClient.h/>
#include <BlynkSimpleEsp32.h>


#include "secrets.h"

void led_pwm(int pwm_value);

byte pwm_pin = 22;

BLYNK_WRITE(V1) //Button Widget is writing to pin V1
{
  led_pwm(param.asInt());
}

void setup()
{
  // Debug console
  Serial.begin(115200);

  ledcAttachPin(pwm_pin, 1);
  ledcSetup(1, 12000, 8);  

  Blynk.begin(auth, ssid, pass);

}

void loop()
{
  Blynk.run();
}

void led_pwm(int pwm_value)
{
  ledcWrite(1, pwm_value);
}
